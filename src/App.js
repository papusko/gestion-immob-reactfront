import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Inscription from './authentication/Inscription';
import Connexion from './authentication/Connexion'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


function App() {
  return (

    <Router>
    <div>
      {/* <ul>
        <li>
          <Link to="/">Connexion</Link>
        </li>
        <li>
          <Link to="/inscription">Inscription</Link>
        </li> */}
        {/* <li>
          <Link to="/dashboard">Dashboard</Link>
        </li> */}
      {/* </ul> */}

      <hr />

      {/*
        Un <Switch> regarde à travers tous ses enfants <Route>
         éléments et rend le premier dont le chemin
         correspond à l'URL actuelle. Utilisez un <Switch> à tout moment
         vous avez plusieurs itinéraires, mais vous n'en voulez qu'un
         à rendre à la fois
      */}
      <Switch>
        <Route exact path="/">
          <Connexion />
        </Route>
        <Route path="/inscription">
          <Inscription />
        </Route>
        {/* <Route path="/dashboard">
          <Dashboard />
        </Route> */}
      </Switch>
    </div>
  </Router>


  );
}

export default App;
