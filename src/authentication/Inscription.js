import React from 'react';
import {useForm} from 'react-hook-form';
import Button from 'react-bootstrap/Button'
import 'bootstrap/dist/css/bootstrap.min.css'
import Footer from '../designComponent/Footer';
import {Link} from 'react-router-dom'


function Inscription() {

    const {register, handleSubmit,  errors} = useForm();
    const  onSubmit = data => console.log(data)

    return (

        <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
            <div class="card card0 border-0">
                <div class="row d-flex">
                    <div class="col-lg-6">
                        <div class="card1 pb-5">
                           
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card2 card border-0 px-4 py-5">
                            <div class="row mb-4 px-3">
                               <h6 class="mb-0 mr-4 mt-2">Sign in with</h6>

                                    <form onSubmit={handleSubmit(onSubmit)}>
                                        <div class="row px-3"> 
                                            <input type="text" name="first_name" defaultValue="ibrahima"/>
                                        </div><br/>
                                        <div class="row px-3">    
                                            <input type="text" placeholder="votre nom" name="last_name" ref={register({required: true})}/>
                                            {errors.last_name && <span>Le nom est obligatoire</span>}
                                        </div><br />
                                        <div class="row px-3">
                                            <input type="text" name="pseudo" placeholder="nom utilisateur" ref={register({required: true})} />
                                            {errors.pseudo && <span>Le pseudo est obligatoire</span>}
                                        </div><br />
                                        <div class="row px-3">
                                            <input type="email" name="email" placeholder="votre adresse mail" ref={register({
                                                required: true,
                                                pattern: {
                                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                                    message: "adresse email invalide"
                                                  }
                                                })} />
                                            {errors.email && <span>L'e-mail est obligatoire</span>}
                                        </div><br />
                                        <div class="row px-3">
                                            <input type="password" name="password" placeholder="mot de passe" ref={register({required: true})} />
                                            {errors.password && <span>Le mot de passe est obligatoire</span>}
                                        </div><br/>

                                        <div class="row px-3">
                                            <input type="password" placeholder="Répéter le mot de passe" name="confirmPasword" ref={register({required: true})} />

                                            {errors.confirmPassword && <span>Veuillez confirmer votre mot de passe</span>}
                                        </div><br/>
                                        <div class="row px-3 mb-4">
                                        <br/><br/>
                                        <div class="row mb-4 px-3"> 
                                            <small class="font-weight-bold">Vous avez déjà un compte? <Link to="/" class="text-danger ">Se connecter</Link></small> </div>
                                        </div>
                                        
                                        <div class="row mb-3 px-3"> 
                                            <Button className="btn btn-success" type="submit" class="btn btn-blue text-center">Enregistrer</Button> 
                                        </div>
                                      
                                        
                                    </form>                                    

                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <Footer/>                                
            
        </div>
    )
}

export default Inscription
