import axios from 'axios';
import React, {useState} from 'react'
import Button from 'react-bootstrap/Button'
import {useForm} from 'react-hook-form';
import Footer from '../designComponent/Footer';
import Nav from '../designComponent/Nav';
import {Link} from 'react-router-dom'





function Connexion() {

    const [email, setEmail] = useState('');
    const [password, setPassword]=useState('');

    const {register,  handleSubmit,  errors} = useForm();
    const onSubmit = e => {
        e.preventDefault()
        axios.post('http://159.89.32.58:7777/api/auth/login/', {email, password}).then(response => {
            console.log(response.status)
    
    
        }).catch(error => {
            console.log(error)
        })
    }








    return (
        <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
            <Nav />
        <div class="card card0 border-0">
            <div class="row d-flex">
                <div class="col-lg-6">
                    <div class="card1 pb-5">
                       
                    </div>
                </div>
                <div className="col-lg-6">
                    <div className="card2 card border-0 px-4 py-5">
                        <div className="row mb-4 px-3">
                           <h6 className="mb-0 mr-4 mt-2">Sign in with</h6>

                                <form onSubmit={e =>{handleSubmit(onSubmit(e))}} >
                                    
                                    <div className="row px-3">
                                        <input type="email" name="email"            placeholder="votre adresse mail"
                                        onChange={e => setEmail(e.target.value)}
                                        ref={register({
                                            required: true,
                                            pattern: {
                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                                message: "adresse email invalide"
                                              }
                                            })}
                                             />
                                        {errors.email && <span>L'e-mail est obligatoire</span>}
                                    </div><br />
                                    <div className="row px-3">
                                        <input type="password" 
                                        name="password"
                                        onChange={e => setPassword(e.target.value)} 
                                        placeholder="mot de passe" ref={register({required: true})} />
                                        {errors.password && <span>Le mot de passe est obligatoire</span>}
                                    </div><br/>

                                    <div className="row px-3 mb-4">
                                    <div className="custom-control custom-checkbox custom-control-inline">
                                        <input id="chk1" type="checkbox" name="chk" className="custom-control-input"/> 

                                        <label for="chk1" className="custom-control-label text-sm">Se souvenir de moi</label>
                                        &nbsp;
                                        <Link to="/" class="ml-auto mb-0 text-sm"> Mot de passe oublié?</Link>    
                                    </div><br/><br/>
                                    <div class="row mb-4 px-3"> 
                                        <small className="font-weight-bold">Vous n'avez pas de compte? <Link to="/inscription" class="text-danger ">S'inscrire'</Link></small> </div>
                                    </div>
                                    
                                    <div className="row mb-3 px-3"> 
                                        <Button className="btn btn-success" type="submit" className="btn btn-blue text-center" >Connexion</Button> 
                                    </div>
                                  
                                    
                                </form>                                    

                        </div>
                    </div>
                </div>
            </div>
        </div>
             <Footer/>                                   
        
    </div>
    )
}

export default Connexion
