import React from 'react'
import { Link } from 'react-router-dom'

function Nav() {
    return (
        <div class="container-fluid px-0">
            <nav class="navbar navbar-expand-md navbar-dark bg-black px-0 justify-content-start"> <Link to="/" class="navbar-brand mr-0" >TeamFree</Link> <span class="fa fa-bell"></span>
                <div class="flex-column d-flex">
                    <h6 class="mb-0 notification">Welcome back Daisy!</h6> <small class="text-muted notify-count">3 notifications are waiting for you</small>
                </div> <button class="navbar-toggler mr-2" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active right"> <Link to="/" class="nav-link" href="#"><span class="fa fa-book"></span><span class="nav-text">Content</span></Link> </li>
                        <li class="nav-item"> <Link to="/" class="nav-link" ><span class="fa fa-group"></span><span class="nav-text">People</span></Link> </li>
                        <li class="nav-item"> <Link class="nav-link" ><span class="fa fa-pie-chart"></span><span class="nav-text">Analytics</span></Link> </li>
                    </ul>
                </div>
            </nav>
        </div>
    )
}

export default Nav
